#!/usr/bin/perl

# Checkhosts by a domain list via ICMP connection
# Use: ./check.pl targets.txt
# Heitor Gouvêa - hi@heitorgouvea.me

use 5.010;
use strict;
use warnings;
use Net::Ping;

sub main {
  my $target = $ARGV[0];

  if ($target) {
    $target =~ s/https:\/\/// || $target =~ s/http:\/\/// || $target =~ s/www.//;

    my $checkHost = Net::Ping -> new( $> ? "tcp" : "icmp", 3);

    if ($checkHost -> ping ($target)) {
      print "$target\n";
    }

    $checkHost -> close();
  }
}

main();
exit;
